package com.example.anhealth;


import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.ArrayList;
import java.util.Date;
//setto i valori (tabelle)
@Entity(tableName = "datiUtente")
public class DatoUtente {

   @PrimaryKey(autoGenerate = true)
   private long id;

   @Embedded(prefix = "battito")
   private Nodo battitoCardiaco;
   @Embedded(prefix = "pressione_minima")
   private Nodo pressioneMinArteriosa;
   @Embedded(prefix = "pressione_massima")
   private Nodo pressioneMaxArteriosa;
   @Embedded(prefix = "temperatura")
   private Nodo temperatura;
   @ColumnInfo(name = "data_inserimento")
   private String dataInserimento;
   @ColumnInfo(name = "note")
   private String note;
   //priorità data dal massimo delle priorità delle info inserite
   @ColumnInfo(name = "prioritaReport")
   private int prioritaReport;




   public long getId() {
      return id;
   }

   public Nodo getPressioneMinArteriosa() {
      return pressioneMinArteriosa;
   }

   public Nodo getPressioneMaxArteriosa() {
      return pressioneMaxArteriosa;
   }

   public Nodo getTemperatura() {
      return temperatura;
   }

   public Nodo getBattitoCardiaco() {
      return battitoCardiaco;
   }

   public String getDataInserimento() {
      return dataInserimento;
   }

   public String getNote() {
      return note;
   }

   public int getPrioritaReport() {
      return prioritaReport;
   }

   public void setTemperatura(Nodo temperatura) {
      this.temperatura = temperatura;
   }

   public void setBattitoCardiaco(Nodo battitoCardiaco) {
      this.battitoCardiaco = battitoCardiaco;
   }
   public void setId(long id) {
      this.id = id;
   }
   public void setPressioneMinArteriosa(Nodo pressioneMinArteriosa) {
      this.pressioneMinArteriosa = pressioneMinArteriosa;
   }

   public void setPressioneMaxArteriosa(Nodo pressioneMaxArteriosa) {
      this.pressioneMaxArteriosa = pressioneMaxArteriosa;
   }

   public void setDataInserimento(String dataInserimento) {
      this.dataInserimento = dataInserimento;
   }

   public void setNote(String note) {
      this.note = note;
   }

   public void setPrioritaReport(int prioritaReport) {
      this.prioritaReport = prioritaReport;
   }
}

