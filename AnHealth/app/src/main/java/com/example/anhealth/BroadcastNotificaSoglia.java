package com.example.anhealth;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.room.Room;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BroadcastNotificaSoglia extends BroadcastReceiver {
    private Double battitoMedia = 0.0;
    private Double pMinMedia = 0.0;
    private Double pMaxMedia = 0.0;
    private Double tempMedia = 0.0;
    private int numeroElementi = 0;
    private Boolean lanciaLaNotifica = false; //variabile che serve per capire se la soglia di quel particoalre parametro è stata superata, cosi posso lanciare la notifica
    private final String CHANNEL_ID = "soglia_notification";
    private List<DatoUtente> datoUtente;
    private Context context;
    private int battitoCardiacoPrio = 0;
    private int tempPrio = 0;
    private int pMinPrio = 0;
    private int pMaxPrio = 0;
    private AppDatabase appDatabase;

    @Override
    public void onReceive(Context context, Intent intent) {
        //ne creo uno qui perchè quello della main activity è morto
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, "datiReportDB").build();

        //CREO/LANCIO LA NOTIFICA SOLO SE è LA GIORNATA CORRETTA PERCHè VA LANCIATA ALLA FINE DEL PERIODO CHE L'UTENTE HA SCELTO NEL PICKER DATA
        //recupero la data dalle shared preferences
        SharedPreferences prefs = context.getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        String dataFinePeriodoMonitoraggio = prefs.getString("dataMonitoraggioParametro", "nessuna data selezionata");
        this.context = context;
        //mi calcolo la data di oggi per confrontarla alla data fine periodo scelta
        Date dataOggi = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String dataDiOggi = df.format(dataOggi);

        if (dataFinePeriodoMonitoraggio.equals(dataDiOggi)) { //se sono uguali creo lancio la notifica
            //List<DatoUtente> datoUtente = MainActivity.appDatabase.dataAccessObject().getDati();
            new AsyncTaskGetDatiForSogliaNotify().execute();
        }
    }
    public void gestisciNotifica(Context context){
        SharedPreferences prefs = context.getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        Float sogliaInserita = prefs.getFloat("sogliaInserita", 70); // 70 è il valore di default
        //mi recupero le medie dei vari valori
        for (DatoUtente dato : datoUtente) {
            Double battitoCardiaco = dato.getBattitoCardiaco().getValore();
            battitoCardiacoPrio = dato.getBattitoCardiaco().getPriorità();
            Double temperatura = dato.getTemperatura().getValore();
            tempPrio = dato.getTemperatura().getPriorità();
            Double pressioneMinArteriosa = dato.getPressioneMinArteriosa().getValore();
            pMinPrio = dato.getPressioneMinArteriosa().getPriorità();
            Double pressioneMaxArteriosa = dato.getPressioneMaxArteriosa().getValore();
            pMaxPrio = dato.getPressioneMaxArteriosa().getPriorità();
            // alla fine del ciclo all'interno delle seguenti variabili ci saranno le somme totali
            battitoMedia = battitoMedia + battitoCardiaco;
            pMinMedia = pMinMedia + temperatura;
            pMaxMedia = pMaxMedia + pressioneMinArteriosa;
            tempMedia = tempMedia + pressioneMaxArteriosa;
            numeroElementi++;
        }
        // medie dei valori
        battitoMedia = battitoMedia / numeroElementi;
        pMinMedia = pMinMedia / numeroElementi;
        pMaxMedia = pMaxMedia / numeroElementi;
        tempMedia = tempMedia / numeroElementi;
        //controllo qual è il parametro inserito dall'utente
        //SharedPreferences prefs = context.getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        String paramtroInserito = prefs.getString("paramtroInserito", ""); // 70 è il valore di default

        //controllo se c'è la soglia è stata superata
        if (paramtroInserito.equals("Battito") && (battitoMedia > sogliaInserita) && (battitoCardiacoPrio>3)) {
            lanciaLaNotifica = true; //uso questo parametro per non copiare incolllare tante volte il codice della creazione della notifica
        } else if (paramtroInserito.equals("Pressione minima") && (pMinMedia > sogliaInserita) && (pMinPrio>3)) {
            lanciaLaNotifica = true;
        } else if (paramtroInserito.equals("Pressione massima") && (pMaxMedia > sogliaInserita)&&(pMaxPrio>3)) {
            lanciaLaNotifica = true;
        } else if (paramtroInserito.equals("Temperatura corporea") && (tempMedia > sogliaInserita)&&(tempPrio>3)) {
            lanciaLaNotifica = true;
        }
        //se la var è stata settata a true, creo la notifica
        if (lanciaLaNotifica == true) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            Intent repeating_intent = new Intent(context, MainActivity.class);//al click rimando
            repeating_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //faccio si che la nuova activity sostiuisca la vecchia
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 101, repeating_intent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID);

            mBuilder.setContentIntent(pendingIntent);
            mBuilder.setSmallIcon(R.drawable.ic_soglia_superata);
            mBuilder.setContentTitle("Hai superato la soglia da te inserita");
            mBuilder.setContentText("Devi fare più attenzione!");
            mBuilder.setPriority(Notification.PRIORITY_MAX);
            mBuilder.setStyle(bigText);
            //mBuilder.setAutoCancel(true); // se l'utente tocca la notifica, essa si chiude subito
            //mBuilder.addAction(R.drawable.ic_launcher_background, "Soglia superata", pendingIntent);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(
                        CHANNEL_ID,
                        "Titolo canale",
                        NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(channel);
                mBuilder.setChannelId(CHANNEL_ID);
                //Log.i("Notify", "Alarm");
            }

            notificationManager.notify(101, mBuilder.build());
        }
    }
    public class AsyncTaskGetDatiForSogliaNotify extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            datoUtente = appDatabase.dataAccessObject().getDati();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            gestisciNotifica(context);
        }
    }
}
