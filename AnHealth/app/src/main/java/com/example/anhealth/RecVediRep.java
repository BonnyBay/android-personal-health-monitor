package com.example.anhealth;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class RecVediRep extends Fragment implements AdapterView.OnItemSelectedListener {

    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter; //adattatore tra i dati nel mio arraylist (vettoreInfo) e la recycler view
    private RecyclerView.LayoutManager mLayoutManager;
    private MyAdapter.MyHolder myHolder;
    //private List<DatoUtente> datoUtente = MainActivity.appDatabase.dataAccessObject().getDati();
    private View schermata;
    private List<DatoUtente> report;

    public RecVediRep() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rec_vedi_rep, container, false);
        schermata = view;
        new AsyncTaskGetDatiFiltrati().execute();
        //costruisco lo spinner per la scelta delle priorità
        Spinner spinner = view.findViewById(R.id.prioritaScelta);
        ArrayAdapter<CharSequence> adapterSpinn = ArrayAdapter.createFromResource(getActivity(), R.array.priorita, android.R.layout.simple_spinner_item);
        adapterSpinn.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterSpinn);
        spinner.setOnItemSelectedListener(this);

        return view;
    }

    public void costruisciRecyclerView(View view) {
        mRecyclerView = view.findViewById(R.id.fragment_rec_vedi_rep);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        //mRecyclerView.setAdapter(mAdapter);


        //controllo se l'utente ha settato la priorità
        SharedPreferences prefs = getActivity().getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        String prioritaTest = prefs.getString("priorita", "nessuna priorita selezionata");
        //se non è settata nessuna priorità, mostro tutti i report inseriti
        if (prioritaTest.equals("nessuna priorita selezionata") || prioritaTest.equals("Nessuna priorita")) {
            report = MainActivity.appDatabase.dataAccessObject().getDati();
            mAdapter = new MyAdapter(report, getActivity()); //getActivity mi servirà per le shared preference in MyAdapter
        } else { //se ha selezionato una priorità
            report = MainActivity.appDatabase.dataAccessObject().getDatiFiltrati(Integer.parseInt(prioritaTest));
            mAdapter = new MyAdapter(report, getActivity());
        }


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String prioritaSelezionata = parent.getItemAtPosition(position).toString();
        SharedPreferences prefs = getActivity().getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        if (!prioritaSelezionata.equals("Seleziona una priorita")) { //solo se è diverso eseguo il codice sottostante, vuol dire che l'utente a selezionato un parametro
            // inserisco il parametro deciso dall'utente nelle shared preference
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("priorita", prioritaSelezionata); //parametro che proviene dallo spinner
            editor.apply();
            //test
            //RECUPERO valore che ho appena inserito nelle shared
            //costruisciRecyclerView(view);
            getFragmentManager().beginTransaction().replace(R.id.fragment_container, new RecVediRep()).commit(); //grafico a torta
            String prioritaTest = prefs.getString("priorita", "nessuna priorita selezionata");
            Toast.makeText(parent.getContext(), "Priorità " + prioritaTest + " selezionata", Toast.LENGTH_LONG).show();
        }else if(prioritaSelezionata.equals("Nessuna priorita")){
            getFragmentManager().beginTransaction().replace(R.id.fragment_container, new RecVediRep()).commit(); //grafico a torta
            //reimposto la priorità a quella "non selezionata" di default
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("priorita", "nessuna priorita selezionata"); //parametro che proviene dallo spinner
            editor.apply();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onStart() {
        super.onStart();
    }



    public class AsyncTaskGetDatiFiltrati extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            costruisciRecyclerView(schermata);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

}
