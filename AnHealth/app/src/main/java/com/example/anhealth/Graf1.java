package com.example.anhealth;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.room.Room;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Graf1 extends Fragment {
    private AppDatabase appDatabase;
    private ArrayList<BarEntry> valoriGraficoBarre = new ArrayList<>();
    private List<DatoUtente> datoUtente;
    public Graf1() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_graf1, container, false);

        appDatabase = Room.databaseBuilder(getActivity(), AppDatabase.class, "datiReportDB").build();
        new AsyncTaskGetDatiReport().execute();
        //setRetainInstance(true); //evito il problema del NULL dato dal cambio della rotazione

        return view;
    }


    //metodo che verifica che il campo sia effettivamente stato riempito
    //ossia che non ci sia il valore double 0.0, identificativo dei campi non riempiti
    public Boolean controllaPresenzaValore(Double datoReportInserito){
        if(datoReportInserito != 0.0){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //prima di ri-popolare il grafico qualora provenga per esempio da SETTINGS, tolgo dal fragment i valori del grafico precedente
        if(!valoriGraficoBarre.isEmpty()){ // se non è vuoto, lo svuoto
            valoriGraficoBarre.clear();
        }
        Context context = getContext();
    }

    public void popolaGrafico(){
        int i = 0;
        SharedPreferences prefs = getActivity().getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        String parametroInserito = prefs.getString("paramtroInserito", "nessun valore selezionato");
        for (DatoUtente dato : datoUtente) {
            //inserisco nel grafico solo i valori di al massimo 7 giorni fa
            //calcolo la data di oggi
            Date dataOggi = Calendar.getInstance().getTime();
            SimpleDateFormat dataOggiFormattata = new SimpleDateFormat("dd-MM-yyyy");
            //String formattedDate = df.format(dataOggi);
            //prendo le varie date (volta per volta) dal database
            String dataProvenienteDalDB = dato.getDataInserimento(); // è in stringa, va convertito in date
            //Log.d("ParScelto ", parametroInserito);
            try {
                Date dataDaControllare = new SimpleDateFormat("dd-MM-yyyy").parse(dataProvenienteDalDB);
                int differenzaTraLeDate = (int) ((dataOggi.getTime() - dataDaControllare.getTime())
                        / (1000 * 60 * 60 * 24));
                //Log.d("valdataSoglia ", differenzaTraLeDate+"");
                if (differenzaTraLeDate < 7) { //se è entro una settimana, mostro i valori
                    String dataReport = dato.getDataInserimento();
                    //prendo il parametro che l'utente vuole monitorare dalle shared preference qualora sia stato settato
                    //se non è ancora stato settato mostro i valori del battito cardiaco

                    if(parametroInserito.equals("Battito")){ //mostro i dati in base al parametro scelto
                        //stampo i dati solo se non sono 0.0, per verificare che siano stati riempiti quei campi
                        double parScelto = dato.getBattitoCardiaco().getValore();
                        if(controllaPresenzaValore(parScelto)) {
                            float parSceltoFloat = (float) parScelto;
                            valoriGraficoBarre.add(new BarEntry(i, parSceltoFloat)); //non accettava stringhe e double, gli passo un float
                            i++;
                        }
                    }else if(parametroInserito.equals("Pressione minima")){
                        double parScelto = dato.getPressioneMinArteriosa().getValore();
                        if(controllaPresenzaValore(parScelto)){
                            float parSceltoFloat = (float) parScelto;
                            valoriGraficoBarre.add(new BarEntry(i, parSceltoFloat));
                            i++;
                        }
                    }else if(parametroInserito.equals("Pressione massima")){
                        double parScelto = dato.getPressioneMaxArteriosa().getValore();
                        if(controllaPresenzaValore(parScelto)){
                            float parSceltoFloat = (float) parScelto;
                            valoriGraficoBarre.add(new BarEntry(i, parSceltoFloat));
                            i++;
                        }
                    }else if(parametroInserito.equals("Temperatura corporea")){
                        double parScelto = dato.getTemperatura().getValore();
                        if(controllaPresenzaValore(parScelto)){
                            float parSceltoFloat = (float) parScelto;
                            valoriGraficoBarre.add(new BarEntry(i, parSceltoFloat));
                            i++;
                        }
                    }else if(parametroInserito.equals("nessun valore selezionato")){//da default mostro il battito cardiaco sul grafico
                        if(dato.getBattitoCardiaco() != null){ //dovrebbe esserci almeno un valore con il battito cardiaco nel DB
                            double battito = dato.getBattitoCardiaco().getValore();
                            if(controllaPresenzaValore(battito)){
                                float battitoFloat = (float) battito;
                                valoriGraficoBarre.add(new BarEntry(i, battitoFloat));
                                i++;
                            }
                        }

                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        BarDataSet barDataSet = new BarDataSet(valoriGraficoBarre, "valoriGraficoBarre");
        barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        barDataSet.setValueTextColor(Color.BLACK);
        barDataSet.setValueTextSize(16f);
        BarChart graficoABarre = getActivity().findViewById(R.id.idGraf1);
        BarData barData = new BarData(barDataSet);
        //per evitare errore di NULL in modalità landascape
        int orientation = getResources().getConfiguration().orientation;
        if (orientation != Configuration.ORIENTATION_LANDSCAPE) {
            graficoABarre.getAxisRight().setDrawGridLines(false);
            graficoABarre.getAxisLeft().setDrawGridLines(false);
            graficoABarre.getXAxis().setDrawGridLines(false);

        } //else sono in LANDSCAPE

        barDataSet.setColors(ColorTemplate.PASTEL_COLORS);
            /*
        graficoABarre.setDrawValueAboveBar(false);
        graficoABarre.setDrawBarShadow(false);
        graficoABarre.setDrawGridBackground(true);
*/
        graficoABarre.setData(barData);
        graficoABarre.invalidate(); //refresho (se no stampava no chart data available quando tornavo indietro dal calendario)
        String parScelto = prefs.getString("paramtroInserito", "nessun valore selezionato");
        graficoABarre.getDescription().setText("Parametro scelto monitorato: "+parScelto);
        graficoABarre.animateY(2000);
    }



    public class AsyncTaskGetDatiReport extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            datoUtente = MainActivity.appDatabase.dataAccessObject().getDati();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            popolaGrafico();
        }
    }
}
