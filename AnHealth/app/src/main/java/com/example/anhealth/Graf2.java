package com.example.anhealth;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.room.Room;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Graf2 extends Fragment {
    private AppDatabase appDatabase;
    private ArrayList<PieEntry> valoriGraficoTorta = new ArrayList<>();
    private List<DatoUtente> datoUtente;
    public Graf2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_graf2, container, false);
        new AsyncTaskGetDatiReportForTorta().execute();
        return view;
    }
    public Boolean controllaPresenzaValore(Double datoReportInserito){
        if(datoReportInserito != 0.0){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        //prima di ri-popolare il grafico qualora provenga per esempio da SETTINGS, tolgo dal fragment i valori del grafico precedente
        if(!valoriGraficoTorta.isEmpty()){ // se non è vuoto, lo svuoto
            valoriGraficoTorta.clear();
        }

        appDatabase = Room.databaseBuilder(getActivity(), AppDatabase.class, "datiReportDB").build();

        Context context = getActivity();


    }

    //metodo che controlla l'intervallo nel quale inserire il valore per il grafico a torta
    /*
    *
    * INTERVALLI:
    * BATTIO < 50 -------------> ritorna 1
    * 50 < BATTITO < 90 -------------> ritorna 2
    * BATTITO > 90 -------------> ritorna 3
    *
    * */
    private ArrayList<Float> minCinquanta = new ArrayList<>();
    private ArrayList<Float> maxNovanta = new ArrayList<>();
    private ArrayList<Float> intermedio = new ArrayList<>();
    private float valoreTotale = 0;
    public void checkIntervallo(float parSceltoFloatInput){
        valoreTotale += parSceltoFloatInput;
        if(parSceltoFloatInput<50){
            minCinquanta.add(parSceltoFloatInput);
            //return 1;
        }else if(parSceltoFloatInput > 50 && parSceltoFloatInput<90){
            intermedio.add(parSceltoFloatInput);
            //return 2;
        }else{ //battito > 90
            maxNovanta.add(parSceltoFloatInput);
            //return 3;
        }
    }

    public void popolaGraficoTorta(){
        PieChart pieChart = getActivity().findViewById(R.id.grafTorta);
        int i = 0;
        SharedPreferences prefs = getActivity().getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        String parametroInserito = prefs.getString("paramtroInserito", "nessun valore selezionato");
        for (DatoUtente dato : datoUtente){
            //inserisco nel grafico solo i valori di al massimo 7 giorni fa
            //calcolo la data di oggi
            Date dataOggi = Calendar.getInstance().getTime();
            SimpleDateFormat dataOggiFormattata = new SimpleDateFormat("dd-MM-yyyy");
            //String formattedDate = df.format(dataOggi);
            //prendo le varie date (volta per volta) dal database
            String dataProvenienteDalDB = dato.getDataInserimento(); // è in stringa, va convertito in date
            try {
                Date dataDaControllare=new SimpleDateFormat("dd-MM-yyyy").parse(dataProvenienteDalDB);
                int differenzaTraLeDate = (int)( (dataOggi.getTime() - dataDaControllare.getTime())
                        / (1000 * 60  * 60 * 24) );
                if (differenzaTraLeDate < 7){ //se è entro una settimana, mostro i valori
                    if(parametroInserito.equals("Battito")){ //mostro i dati in base al parametro scelto
                        //stampo i dati solo se non sono 0.0, per verificare che siano stati riempiti quei campi
                        double parScelto = dato.getBattitoCardiaco().getValore();
                        if(controllaPresenzaValore(parScelto)) {
                            float parSceltoFloat = (float) parScelto;
                            //valoriGraficoTorta.add(new PieEntry(parSceltoFloat,i ));
                            checkIntervallo(parSceltoFloat);



                            i++;
                        }
                    }else if(parametroInserito.equals("Pressione minima")){
                        double parScelto = dato.getPressioneMinArteriosa().getValore();
                        if(controllaPresenzaValore(parScelto)){
                            float parSceltoFloat = (float) parScelto;
                            //valoriGraficoTorta.add(new PieEntry(parSceltoFloat,i ));
                            checkIntervallo(parSceltoFloat);
                            i++;
                        }
                    }else if(parametroInserito.equals("Pressione massima")){
                        double parScelto = dato.getPressioneMaxArteriosa().getValore();
                        if(controllaPresenzaValore(parScelto)){
                            float parSceltoFloat = (float) parScelto;
                            //valoriGraficoTorta.add(new PieEntry(parSceltoFloat,i ));
                            checkIntervallo(parSceltoFloat);
                            i++;
                        }
                    }else if(parametroInserito.equals("Temperatura corporea")){
                        double parScelto = dato.getTemperatura().getValore();
                        if(controllaPresenzaValore(parScelto)){
                            float parSceltoFloat = (float) parScelto;
                            //valoriGraficoTorta.add(new PieEntry(parSceltoFloat,i ));
                            checkIntervallo(parSceltoFloat);
                            i++;
                        }
                    }else if(parametroInserito.equals("nessun valore selezionato")){//da default mostro il battito cardiaco sul grafico
                        double battito = dato.getBattitoCardiaco().getValore();
                        if(controllaPresenzaValore(battito)){
                            float battitoFloat = (float) battito;
                            //valoriGraficoTorta.add(new PieEntry(battitoFloat,i ));
                            checkIntervallo(battitoFloat);
                            i++;
                        }
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            i++;
        }
        //calcolo i valori in percentuale SE IL VETTORE NON è VUOTO
        float somma = 0;
        if(!minCinquanta.isEmpty()){ //controllo che non sia vuoto
            for(int j = 0; j<minCinquanta.size(); j++){
                somma = somma + minCinquanta.get(j);
            }
            float valPercentualeMinCinquanta = (somma/valoreTotale)*100; //i è il numero dei valori inseriti
            //aggiungo il valore al grafico a torta
            valoriGraficoTorta.add(new PieEntry(valPercentualeMinCinquanta,1 ));
        }


        float somma2 = 0;
        if(!maxNovanta.isEmpty()){ //controllo che non sia vuoto
            for(int j = 0; j<maxNovanta.size(); j++){
                somma2 = somma2 + maxNovanta.get(j);
            }
            float valPercentualeMaxNovanta = (somma2/valoreTotale)*100; //i è il numero dei valori inseriti
            valoriGraficoTorta.add(new PieEntry(valPercentualeMaxNovanta,2 ));
        }



        float somma3 = 0;
        if(!intermedio.isEmpty()){ //controllo che non sia vuoto
            for(int j = 0; j<intermedio.size(); j++){
                somma3 = somma3 + intermedio.get(j);
            }
            float valPercentualeIntermedio = (somma3/valoreTotale)*100; //i è il numero dei valori inseriti

            valoriGraficoTorta.add(new PieEntry(valPercentualeIntermedio,3 ));
        }



        PieDataSet pieDataSet = new PieDataSet(valoriGraficoTorta, "Legenda");
        pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        pieDataSet.setValueTextColor(Color.BLACK);
        pieDataSet.setValueTextSize(16f);
        //pieDataSet.setColors(ColorTemplate.PASTEL_COLORS); COLORI GRAFICO A TORTA


        final int[] MY_COLORS = {Color.rgb(124,252,0), Color.rgb(0,100,0), Color.rgb(50,205,50)};
        ArrayList<Integer> colors = new ArrayList<Integer>();

        for(int c: MY_COLORS) colors.add(c);

        pieDataSet.setColors(colors);

        PieData pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieChart.invalidate();
        pieChart.getDescription().setEnabled(false);


        LegendEntry legendEntryA = new LegendEntry();
        legendEntryA.label = "BATTITO < 50";
        LegendEntry legendEntryB = new LegendEntry();
        legendEntryB.label = "50 < BATTITO < 90";
        LegendEntry legendEntryC = new LegendEntry();
        legendEntryC.label = "BATTITO > 90";

        legendEntryA.formColor = Color.rgb(124,252,0);
        legendEntryC.formColor = Color.rgb(0,100,0);
        legendEntryB.formColor = Color.rgb(50,205,50);

        Legend legend = pieChart.getLegend();
        legend.setEnabled(true);
        legend.setCustom(Arrays.asList(legendEntryA,legendEntryB,legendEntryC));

        //pieChart.getLegend().setPosition(Legend.LegendPosition.ABOVE_CHART_LEFT);
        pieChart.getLegend().setOrientation(Legend.LegendOrientation.VERTICAL);
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);

        String parScelto = prefs.getString("paramtroInserito", "nessun valore selezionato");
        pieChart.setCenterText("Parametro: "+parScelto);
        pieChart.setCenterTextSize(20f);

        pieChart.animate();
    }


    public class AsyncTaskGetDatiReportForTorta extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            datoUtente = MainActivity.appDatabase.dataAccessObject().getDati();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            popolaGraficoTorta();
        }
    }
}
