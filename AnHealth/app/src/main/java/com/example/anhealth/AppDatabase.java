package com.example.anhealth;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

//classe che rappresenta il Database
@Database(entities = {DatoUtente.class}, version = 1)

public abstract class AppDatabase extends RoomDatabase {
    public abstract DataAccessObject dataAccessObject();
}
