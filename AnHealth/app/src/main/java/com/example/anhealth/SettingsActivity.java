package com.example.anhealth;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.FragmentManager;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class SettingsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Button gestisciNotifica; //bottone per settare l'orario delle notifiche
    private Button inserisciSoglia;
    private EditText ore, minuti; //devo aggiungere i secondi
    private EditText sogliaInserita; //devo aggiungere i secondi
    private EditText parametroInserito;
    private Boolean notificaSettataDaUtente = false;
    private boolean flagReportInseritiOggi = false; //variabile che mi dice se oggi è stato inserito un report o meno
    private TextView dataSceltaMonitoraggio;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private Spinner spinner;
    private Bundle savInState;
    private FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        //lancio la notifica nel caso l'utente non setti un orario in particolare
        //gli passo dei valori di default
        //setUpNotification(17,30); // faccio già questa chiamata nella MainActivity
        savInState = savedInstanceState;
        ore = findViewById(R.id.oraText);
        minuti = findViewById(R.id.minutiText);
        gestisciNotifica = findViewById(R.id.settaNotifiche);
        //setto l'orario della notifica riguardante l'AGGIUNTA QUOTIDIANA DI ALMENO UN REPORT
        gestisciNotifica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //converto le ore da texedit a string a intero
                String oreString = ore.getText().toString();
                final int oreInt = Integer.parseInt(oreString);
                //converto i minuti da texedit a string a intero
                String minutiString = minuti.getText().toString();
                final int minutiInt = Integer.parseInt(minutiString);
                setUpNotification(oreInt, minutiInt);
            }
        });
        fragmentManager = getSupportFragmentManager();
        // gestisco il parametro che vuole monitorare l'utente e lo salvo nelle shared preference (metodo setOnItemSelected)
        spinner = findViewById(R.id.parametroInserito);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.parametri, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        // gestisco il periodo di monitoraggio del parametro con il data picker
        dataSceltaMonitoraggio = (TextView) findViewById(R.id.dataScelta);
        dataSceltaMonitoraggio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendario = Calendar.getInstance();
                int anno = calendario.get(Calendar.YEAR);
                int mese = calendario.get(Calendar.MONTH);
                int giorno = calendario.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(SettingsActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        anno, mese, giorno);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String dataSelezionata = dayOfMonth + "-" + month + "-" + year;
                //serve la data in formato Date per avere il formato 00-00-0000 e non 12-2-2020, mi serve lo 0!
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                Date startDate;
                try {
                    startDate = df.parse(dataSelezionata);
                    String newDateString = df.format(startDate); //inserirò questa data nelle shared preference è quella nel formato: 00-00-0000
                    dataSceltaMonitoraggio.setText(dataSelezionata);
                    SharedPreferences prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("dataMonitoraggioParametro", newDateString);
                    editor.apply();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        };

        //Gestisco l'action quando l'utente vuole impostare un valor soglia predefinito
        inserisciSoglia = findViewById(R.id.impostaSogliaBtn);
        sogliaInserita = findViewById(R.id.sogliaInserita);

        //eseguito solo al click del bottone con variabile booleana
        spinner.setOnItemSelectedListener(this);

        inserisciSoglia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //poi proseguo con la gestione della soglia inserita
                    String sogliaInseritaString = sogliaInserita.getText().toString();
                    final Float sogliaInseritaFloat = Float.parseFloat(sogliaInseritaString);
                    // inserisco il valore soglia deciso dall'utente nelle shared preference
                    SharedPreferences prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putFloat("sogliaInserita", sogliaInseritaFloat);
                    editor.apply();
                    //recupero il parametro inserito
                    String paramtroInsert = prefs.getString("paramtroInserito", "nessun valore selezionato");
                    setUpNotificationSoglia();

                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });
    }

    // metodo che lancia la notifica passando i parametri: ore:minuti:secondi
    // qualora l'utente li abbia inseriti, se no gli passa i parametri
    // che stabilisco io di default
    public void setUpNotification(int oreInt, int minutiInt) {
        Calendar sceltaOrarioCalendario = Calendar.getInstance(); //mi servirà per trovare l'orario corretto
        sceltaOrarioCalendario.set(Calendar.HOUR_OF_DAY, oreInt);
        sceltaOrarioCalendario.set(Calendar.MINUTE, minutiInt);
        sceltaOrarioCalendario.set(Calendar.SECOND, 0);
        Toast.makeText(getApplicationContext(), "Orario in cui riceverai la notifica: " + oreInt + ":" + minutiInt, Toast.LENGTH_LONG).show();

        SharedPreferences prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("orarioOre", oreInt);
        editor.putInt("orarioMin", minutiInt);
        //variabile utile a capire se l'utente ha settato la notifica cosi non lancio più la notifica della mainactivity
        editor.putString("notifUserSetted", "settata");
        editor.apply();

        Intent intent = new Intent(getApplicationContext(), NoticationBroadcastreceiver.class);
        intent.setAction("MY_NOTIFICATION_MESSAGE");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        //considero anche se il device va in sleep mode
        //come secondo parametro gli passo la data/ora scelta dall'utente
       // Log.d("tag", " --------------Sono in set up notification -------");
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, sceltaOrarioCalendario.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    public void setUpNotificationSoglia() {
        Calendar sceltaOrarioCalendario = Calendar.getInstance(); //mi servirà per trovare l'orario corretto
        Toast.makeText(getApplicationContext(), "Notifica impostata ", Toast.LENGTH_LONG).show();
        sceltaOrarioCalendario.set(Calendar.HOUR_OF_DAY, 12);
        sceltaOrarioCalendario.set(Calendar.MINUTE, 51);
        sceltaOrarioCalendario.set(Calendar.SECOND, 30);
        Intent intent = new Intent(getApplicationContext(), BroadcastNotificaSoglia.class);
        intent.setAction("MY_NOTIFICATION_MESSAGE");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        //considero anche se il device va in sleep mode
        //come secondo parametro gli passo la data/ora scelta dall'utente
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, sceltaOrarioCalendario.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String parametro = parent.getItemAtPosition(position).toString();
        if(!parametro.equals("Seleziona un parametro")){ //solo se è diverso eseguo il codice sottostante, vuol dire che l'utente a selezionato un parametro
            // inserisco il parametro deciso dall'utente nelle shared preference
            SharedPreferences prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("paramtroInserito", parametro); //parametro che proviene dallo spinner
            editor.apply();
            //test
            //RECUPERO valore che ho appena inserito nelle shared
            String paramtroInseritotest = prefs.getString("paramtroInserito", "nessun valore selezionato");
            Toast.makeText(parent.getContext(), paramtroInseritotest + " selezionato", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    //metodo mandato in esecuzione quando premo sul bottone "Imposta valore soglia"
    public void settaParametro(Spinner spinner){
        spinner.setOnItemSelectedListener(SettingsActivity.this); // va fatto solo quando premo il pulsante, non tutte le volte che entro nell'activity SETTINGS
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();

        //getFragmentManager().popBackStack();
        SettingsActivity.this.finish();
       // super.onBackPressed();

    }
}
