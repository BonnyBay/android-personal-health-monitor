package com.example.anhealth;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class AggiornaReport extends Fragment {
    private EditText battito, pMin, pMax, temperatura, note, id;
    private Button salva;
    private int numeroCampiCompletati = 0;
    private int prioritaTotale = 0;

    public AggiornaReport() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_aggiorna_report, container, false);
        battito = view.findViewById(R.id.aggiorna_battitoCardiacoText);
        pMin = view.findViewById(R.id.aggiorna_pressioneMinText);
        pMax = view.findViewById(R.id.aggiorna_pressioneMaxText);
        temperatura = view.findViewById(R.id.aggiorna_temperaturaText);
        note = view.findViewById(R.id.aggiorna_noteText);
        //recupero l'id passato dalla recycler view
        String stringIDricevuto = getArguments().getString("ID");
        //converto l'id in long
        final Long idRicevuto = Long.parseLong(stringIDricevuto);
        // Log.d("ID_RICEVUTO ",idRicevuto+"");

        DatoUtente datoUtente = new DatoUtente();
        salva = view.findViewById(R.id.aggiorna_aggiungiReport);
        salva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double battitoCardiacoDouble = 0.0, pressioneMinArteriosaDouble = 0.0, pressioneMaxArteriosaDouble = 0.0, temperaturaDouble = 0.0;

                String noteString = "Nessuna nota";
                DatoUtente datoUtente = new DatoUtente();
                datoUtente.setId(idRicevuto); //preso dal bundle
                if (battito.getText().length() != 0) {
                    numeroCampiCompletati++;
                    battitoCardiacoDouble = Double.parseDouble(battito.getText().toString());
                    prioritaTotale = 5;
                } else { // se non è stato inserito alcun valore, lo inserisco di default mettendo 0.0. Eviterò errori NULL
                    battitoCardiacoDouble = 0.0;
                }
                Nodo battitoCardiaco = new Nodo(battitoCardiacoDouble, 5);
                datoUtente.setBattitoCardiaco(battitoCardiaco);

                if (pMin.getText().length() != 0) {
                    numeroCampiCompletati++;
                    pressioneMinArteriosaDouble = Double.parseDouble(pMin.getText().toString());
                    //devo controllare se la priorità totale è già stata settata
                    if (prioritaTotale == 0) { //se non è stata settata la setto
                        prioritaTotale = 2;
                    }
                } else {
                    pressioneMinArteriosaDouble = 0.0;
                }
                Nodo pressMinObjc = new Nodo(pressioneMinArteriosaDouble, 2);
                datoUtente.setPressioneMinArteriosa(pressMinObjc);

                if (pMax.getText().length() != 0) {
                    numeroCampiCompletati++;
                    pressioneMaxArteriosaDouble = Double.parseDouble(pMax.getText().toString());
                    Nodo pressMaxObjc = new Nodo(pressioneMaxArteriosaDouble, 3);
                    datoUtente.setPressioneMaxArteriosa(pressMaxObjc);
                    if (prioritaTotale == 0 || 3 > prioritaTotale) {
                        prioritaTotale = 3;
                    }
                } else {
                    pressioneMaxArteriosaDouble = 0.0;
                }
                Nodo pressMaxObjc = new Nodo(pressioneMaxArteriosaDouble, 3);
                datoUtente.setPressioneMaxArteriosa(pressMaxObjc);

                if (temperatura.getText().length() != 0) {
                    numeroCampiCompletati++;
                    temperaturaDouble = Double.parseDouble(temperatura.getText().toString());
                    if (prioritaTotale == 0 || 4 > prioritaTotale) {
                        prioritaTotale = 4;
                    }
                } else {
                    temperaturaDouble = 0.0;
                }
                Nodo tempObjc = new Nodo(temperaturaDouble, 4);
                datoUtente.setTemperatura(tempObjc);

                if (note.getText().length() != 0) {
                    numeroCampiCompletati++;
                    noteString = note.getText().toString();
                    datoUtente.setNote(noteString);
                }

                if (numeroCampiCompletati < 2) {
                    Toast.makeText(getActivity(), "Devi riempire almeno due campi",
                            Toast.LENGTH_LONG).show();
                } else {
                    Date dataOggi = Calendar.getInstance().getTime();
                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    String formattedDate = df.format(dataOggi);

                    //setto la priorità totale aggiornata
                    datoUtente.setPrioritaReport(prioritaTotale);
                    datoUtente.setDataInserimento(formattedDate); //la data diventerà data di aggiornamento
                    new AsyncTaskAggiornaReport().execute(datoUtente);

                }
                //svuoto i campi
                numeroCampiCompletati = 0;
                battito.setText("");
                pMin.setText("");
                pMax.setText("");
                temperatura.setText("");
                note.setText("");
            }

        });
        return view;
    }

    public class AsyncTaskAggiornaReport extends AsyncTask<DatoUtente, Void, Void> {

        @Override
        protected Void doInBackground(DatoUtente... datoUtentes) {
            MainActivity.appDatabase.dataAccessObject().aggiornaReport(datoUtentes[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getActivity(), "Report aggiornato",
                    Toast.LENGTH_LONG).show();
            MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new RecVediRep()).commit();//non lo aggiungo allo stack

        }
    }

}
