package com.example.anhealth;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;



public class AggiungiDatoUtenteFragment extends Fragment {
    private Button inviaReport;
    private EditText battitoCardiaco, pressioneMinArteriosa, pressioneMaxArteriosa, temperatura, note;
    private int numeroCampiCompletati = 0; //variabile utile a verificare che almeno due campi siano stati riempiti
    private int prioritaTotale = 0; //riassuntiva delle info. Assegnata ad ogni report
    public AggiungiDatoUtenteFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_aggiungi_dato_utente, container, false);
        battitoCardiaco = view.findViewById(R.id.battitoCardiacoText);
        //battitoCardiaco.setEnabled(false);
        pressioneMinArteriosa = view.findViewById(R.id.pressioneMinText);
        pressioneMaxArteriosa = view.findViewById(R.id.pressioneMaxText);
        note = view.findViewById(R.id.noteText);
        temperatura = view.findViewById(R.id.temperaturaText);

        inviaReport = view.findViewById(R.id.aggiungiReport);
        inviaReport.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //devo controllare che non siano vuoti
                //inoltre servono almeno due dati inseriti
                Double battitoCardiacoDouble= 0.0, pressioneMinArteriosaDouble= 0.0, pressioneMaxArteriosaDouble= 0.0, temperaturaDouble = 0.0;
                String noteString = "Nessuna nota";
                DatoUtente datoUtente = new DatoUtente();
                if(battitoCardiaco.getText().length() != 0){
                    numeroCampiCompletati++;
                    battitoCardiacoDouble = Double.parseDouble(battitoCardiaco.getText().toString());
                    prioritaTotale = 5;
                }
                Nodo battitoCardiacoObjc = new Nodo(battitoCardiacoDouble,5);
                datoUtente.setBattitoCardiaco(battitoCardiacoObjc);

                if(pressioneMinArteriosa.getText().length() != 0){
                    numeroCampiCompletati++;
                    pressioneMinArteriosaDouble = Double.parseDouble(pressioneMinArteriosa.getText().toString());

                    //devo controllare se la priorità totale è già stata settata
                    if(prioritaTotale == 0){ //se non è stata settata la setto
                        prioritaTotale = 2;
                    }
                }

                Nodo pressMinObjc = new Nodo(pressioneMinArteriosaDouble,2);
                datoUtente.setPressioneMinArteriosa(pressMinObjc);
                if(pressioneMaxArteriosa.getText().length() != 0){
                    numeroCampiCompletati++;
                    pressioneMaxArteriosaDouble = Double.parseDouble(pressioneMaxArteriosa.getText().toString());
                    if(prioritaTotale == 0 || 3 > prioritaTotale){
                        prioritaTotale = 3;
                    }
                }

                Nodo pressMaxObjc = new Nodo(pressioneMaxArteriosaDouble,3);
                datoUtente.setPressioneMaxArteriosa(pressMaxObjc);
                if(temperatura.getText().length() != 0){
                    numeroCampiCompletati++;
                    temperaturaDouble = Double.parseDouble(temperatura.getText().toString());
                    if(prioritaTotale == 0 || 4 > prioritaTotale){
                        prioritaTotale = 4;
                    }
                }
                Nodo tempObjc = new Nodo(temperaturaDouble,4);
                datoUtente.setTemperatura(tempObjc);

                if(note.getText().length() !=0){
                    //numeroCampiCompletati++; ---------> LE NOTE SONO OPZIONALI, QUINDI NON LO CONTO COME CAMPO COMPLETATO
                    noteString = note.getText().toString();
                    datoUtente.setNote(noteString);
                }else{
                    datoUtente.setNote(noteString);
                }
                if(numeroCampiCompletati<2){
                    Toast.makeText(getActivity(), "Devi riempire almeno 2 campi",
                            Toast.LENGTH_LONG).show();
                }else{
                    //gestione data del report
                    //inserisco report solo nella data corrente, non posso inserire report per date diverse da quelle di oggi
                    Date dataOggi = Calendar.getInstance().getTime();
                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    String formattedDate = df.format(dataOggi);

                    datoUtente.setDataInserimento(formattedDate);
                    //datoUtente.setDataInserimento("07-05- 2020");

                    //setto la priorità totale al report
                    datoUtente.setPrioritaReport(prioritaTotale);

                    new AsyncTaskAggiungiReport().execute(datoUtente);
                   // MainActivity.appDatabase.dataAccessObject().aggiungiDatoUtente(datoUtente);

                    Toast.makeText(getActivity(), "Report aggiunto",
                            Toast.LENGTH_LONG).show();
                }

                //svuoto i campi
                prioritaTotale = 0;
                numeroCampiCompletati=0;
                battitoCardiaco.setText("");
                pressioneMinArteriosa.setText("");
                pressioneMaxArteriosa.setText("");
                temperatura.setText("");
                note.setText("");
            }
        });
        return view;
    }

    public class AsyncTaskAggiungiReport extends AsyncTask<DatoUtente, Void, Void>{

        @Override
        protected Void doInBackground(DatoUtente... datoUtentes) {
            MainActivity.appDatabase.dataAccessObject().aggiungiDatoUtente(datoUtentes[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getActivity(), "Report aggiunto",
                    Toast.LENGTH_LONG).show();
        }
    }
}
