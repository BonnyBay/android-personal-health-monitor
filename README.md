# Personal health monitor

In the following project the student is required to implement an interactive application for tracking personal information about his/her health to be saved into daily reports. In particular, the application should be able to manage the reports within a calendar, send notifications and plot the data according to specific filters.

# Author

michele.bonini2@studio.unibo.it
